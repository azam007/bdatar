<?php
$alas = isset($_POST['alas']) ? $_POST['alas'] : NULL;
$tinggi = isset($_POST['tinggi']) ? $_POST['tinggi'] : NULL;
$sisi = isset($_POST['sisi']) ? $_POST['sisi'] : NULL;
if ($submit) {
    if ($alas == NULL || $tinggi == NULL) {
        $notif = notif('gagal','Silahkan Isi dengan benar!');
    }else {
        if (nomorBukan($alas) == true && nomorBukan($tinggi) == true) {
            if ($sisi == NULL) {
                 $sisi = round(sqrt(pow($alas/2, 2)+pow($tinggi, 2))); //pytagoras
            }else{
                   $sisi = $_POST['sisi'];
            }
            $kel = $alas + $sisi + $sisi; 
            $luas = 1/2*$alas*$tinggi;
            $rumus = ['3 x alas','1/2 x alas x tinggi'];
            $hasil = hasil($kel, $luas, $rumus);
        }else{
            $notif = notif('gagal','Yang Anda masukkan bukanlah Angka!');
        }
    }
}