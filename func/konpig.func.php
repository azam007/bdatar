<?php
$bdatar = isset($_GET['bangundatar']) ? $_GET['bangundatar'] : NULL;
$header = 'includes/header.php';
$footer = 'includes/footer.php';
$submit = isset($_REQUEST['submit']) ? $_REQUEST['submit'] : NULL;
$hasil = ""; $notif = "";

if ($bdatar && file_exists("func/{$bdatar}.func.php")) {
    require_once "func/{$bdatar}.func.php";
    $page = "includes/bdatar/{$bdatar}.inc.php";
    $title = ucwords(str_replace("_", " ", $bdatar)).': Luas dan Keliling';
}else{
    $page = 'includes/view.inc.php';
    $title = 'Menghitung Bangun Datar';
}

function nomorBukan($cek){
    if (is_numeric($cek)) {
        return true;
    }else {
        return false;
    }
}
function hasil($kel, $luas, $rumus){
    return "<span>Keliling: {$kel}cm</span>
            <span>Luas: {$luas}cm<sup>2</sup></span><br/>
            dengan rumus:<br/>
            Keliling = $rumus[0]<br/>
            Luas = $rumus[1]";
}
function notif($x,$text){
    return "<div class='$x'>$text</div>
    <meta http-equiv=\"refresh\" content=\"1;url=". $_SERVER[REQUEST_URI] . "#hasil\">";
}
