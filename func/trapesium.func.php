<?php
$alas = isset($_POST['alas']) ? $_POST['alas'] : NULL;
$sisi_a = isset($_POST['sisi_a']) ? $_POST['sisi_a'] : NULL;
$sisi_b = isset($_POST['sisi_b']) ? $_POST['sisi_b'] : NULL;
$tinggi = isset($_POST['tinggi']) ? $_POST['tinggi'] : NULL;
if ($submit) {
    if ($alas == NULL || $tinggi == NULL || $sisi_a == NULL || $sisi_b == NULL) {
        $notif = notif('gagal','Silahkan Isi dengan benar!');
    }else {
        if (nomorBukan($alas) == true &&
            nomorBukan($sisi_a) == true &&
            nomorBukan($sisi_b) == true &&
            nomorBukan($tinggi) == true) {
            $kel = $alas + $sisi_a + ($sisi_b*2); 
            $luas = 1/2*($alas+$sisi_a)*$tinggi;
            $rumus = ['2 x (Panjang + Lebar)','Panjang x Lebar'];
            $hasil = hasil($kel, $luas, $rumus);
        }else{
            $notif = notif('gagal','Yang Anda masukkan bukanlah Angka!');
        }
    }
}