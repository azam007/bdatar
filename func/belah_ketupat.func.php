<?php
$diagonal_1 = isset($_POST['diagonal_1']) ? $_POST['diagonal_1'] : NULL;
$diagonal_2 = isset($_POST['diagonal_2']) ? $_POST['diagonal_2'] : NULL;
$sisi = isset($_POST['sisi']) ? $_POST['sisi'] : NULL;
if ($submit) {
    if ($diagonal_1 == NULL || $diagonal_2 == NULL || $sisi == NULL) {
        $notif = notif('gagal','Silahkan Isi dengan benar!');
    }else {
        if (nomorBukan($diagonal_1) == true && nomorBukan($diagonal_2) == true && nomorBukan($sisi) == true) {
            $kel = 4*$sisi; 
            $luas = 1/2*$diagonal_1*$diagonal_2;
            $rumus = ['4 x Sisi','1/2 x Diagonal 1 x Diagonal 2'];
            $hasil = hasil($kel, $luas, $rumus);
        }else{
            $notif = notif('gagal','Yang Anda masukkan bukanlah Angka!');
        }
    }
}