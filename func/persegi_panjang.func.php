<?php
$panjang = isset($_POST['panjang']) ? $_POST['panjang'] : NULL;
$lebar = isset($_POST['lebar']) ? $_POST['lebar'] : NULL;

if ($submit) {
    if ($panjang == NULL || $lebar == NULL) {
        $notif = notif('gagal','Silahkan Isi dengan benar!');
    }else {
        if (nomorBukan($panjang) == true && nomorBukan($lebar) == true) {
            $kel = 2*($panjang+$lebar);
            $luas = $panjang*$lebar;
            $rumus = ['2 x (Panjang + Lebar)','Panjang x Lebar'];
            $hasil = hasil($kel, $luas, $rumus);

            $notif="";
        }else{
            $notif = notif('gagal','Yang Anda masukkan bukanlah Angka!');
        }
    }
}
